# Elementum
## Overview
This game is inspired by the story of Horizon Zero Dawn. It tells the story of a party of heroes from Aloo who save the world from elemental destruction.

The story plays out over several chapters below.

## The Ascension Festival
[Session Notes](sessions/1_ascension_festival.md)

The party celebrates their ascension into full guild membership and as protectors of Aloo. This year the festival is also host to a diplomatic delegation from Ardor in the hopes of garnering better relations between the two city-states after years of warfare. The festival is attacked by forces unknown. When the party wakes in the presence of Aloomatr (healed of their wounds), they are chosen by the matriarchs to pursue the attackers to discover who they are, why they attacked, and if they will return. They receive the blessing of Aloomatr.

## Chasing Fire
[Session Notes](sessions/2_chasing_fire.md)

The party pursues the attackers to the edge of Aloo territory. There with the help of the war chief they track down the war party that attacked the festival. They find a letter on the commander of the war party describing the blessing of Aloomatr and it's signed by Akali who was part of the diplomatic mission.

## Ardor Prime
[Session Notes](sessions/3_ardor_prime.md)

The party journeys to Ardor Prime in order to track down Akali. They meet with the Solnatus who is surprisingly deferential. He informs the party of who Akali is. The party tracks him down and Akali comes clean. He explains that his family is being held hostage by a cult of fire worshippers called the Zaal who worship an evil fire god called Surtr. In exchange for their assistance, he will tell the party of a Zaal excavation site.

## Digging into the Past
[Session Notes](sessions/4_digging_past.md)

The party search the Zaal excavation site (the lost laboratory of Gimble Wilkes) and discover a series of illusions left behind by a great wizard named Gimble Wilkes. He describes the world of the ancients and how it was ending. The only hope is the Genetrix. After the party defeats a giant unearthed fire elemental (raised by Zaal cultists) they meet Farid who sends them to the ruins of the old Fire Kingdom.

## Imperii Ignis in Ruinas
[Session Notes](sessions/5_imperii_ignis_ruinas.md)

The party explores a dungeon consisting of the remnants of the ancient capitol of the Fire Kingdom. They learn of the sacrifice of the people (and meet some!) to ensure the time needed to build the Genetrix. They also learn the location of where the Genetrix was built, see a design of it that matches the blessing of Aloomatr, and find a second piece of the Genetrix Centrum.

## Preamble to the Final Act
[Session Notes](sessions/5b_preamble.md)

Return to Aloo and decide where to go.

## The Cradle of Life
[Session Notes](sessions/6_cradle_of_life.md)

The party journeys to the Cradle of Life surrounded by the mountains that circumscribe the North Pole. There they learn to key to the mystery of the Ancients and the Genetrix.

## The Great Forge of the Ancients
[Session Notes](sessions/7_great_forge.md)

The party invades the base of the Zaal to recover the third piece of the Genetrix and confront the head of the Zaal.

## Attack on Aloo
[Session Notes](sessions/8_attack_aloo.md)

With the Genetrix completed the party rushes back to Aloo to head off the cult before it attacks. Them must defend the city and bring Surtr to the Genetrix.