# The Ascension Festival
## Objectives
- Introduce the players to Aloo
- Introduce the characters to an Ardorian
- Kick off the plot with the attack
- Send the characters out into the world

## Motivations
This is the first session so the main motivation needs to be establishing some attachment to the 
city. This will motivate the players to actually seek out the attackers after the attack. 

## Scenes

### Outline
- [Exposition](#Exposition)
- [Enter the ranks](#Enter_the_Ranks)
- [Ascension](#Ascension)
- [Attack in the Night](#Attack_in_the_Night)
- [Gift of Aloomatr](#Gift_of_Aloomatr)

### Exposition
```
You and your fellow graduating novitiates have all gathered before the great temple of Aloo. 
The temple is situated at the only entrance to the cradle of life, the mysterious mountains that 
cover the north pole, above which is the Asmaniji: the sky lights. You have come to celebrate your 
full indoctrination into your respective guilds. The Festival of the Ascension is held yearly at the 
height of the summer solstice and is considered one of the most holy times of the year, second only 
to the Aloo Niaz, the time of atonement. Everyone in the city participates in the festival and the 
Order opens its pockets freely to provide food and entertainment for the entire city. You can hear 
the rambunctious roar of the city all around you as people celebrate.
```

### Enter the Ranks
Objectives:
- Get the characters together in the same group
- Initiation challenge in exchange for a boon
- Series of quick encounters to make the city real and meaningful to characters

#### Scene Setting
```
You stand upon the square of Aloomatr, where worshippers come to be lead in prayer each week by the
matriarchs. Today however, only graduating novitiates stand here. The holy mother Terasa, matriarch
of the Earth, speaks to you all, welcoming you to the Order and the guilds. She thanks you for the
oaths of service to Aloo you have taken this day and blesses you all.

After she has stepped away, a man in ornate cyan robes takes her place and instructs you that in a
moment you will all be split into groups of five for a contest. The winners will each receive a boon 
from the matriarchs.

The contest is an egg hunt. Each team will race to search the city together and try and collect one 
of three elemental eggs that were hidden the night before. Each team that finds an egg and returns 
it to the temple will be able to request a boon from the matriarch who represents the element found.

You are brought together and given a few moments to introduce yourselves and formulate your plan.
```

Use the time pool mechanic. Each action the group takes will cover a period of ten minutes and adds
a die to the time pool. Each time the pool reaches six there's a chance another team will find an 
egg. Count the number of 1s rolled in the pool:
- 1 - First egg has been found
- 3 - Second egg has been found
- 5 - Third egg found!

Possible Actions:
- Move from one area of the city to another
- Search an area (doesn't do much but advance the plot)
- Speak to celebrants for clues (this takes no time and removes a die from the pool)

Player Hint!
The city has nine districts:
- Temple
- Guild Headquarters
- Artisan's Square
- Greater Marketplace
- Lesser Marketplace
- Merchant's Plaza
- Upper Residential
- Lower Residential
- Gate

#### Hunt FAQ
- Where are the eggs? Potentially anywhere inside the city, up to the walls. Most of the city 
participates in the hunt, including people's shops and homes. However, anyone with a banner or 
symbol of a red circle is not participating and their place of business or home should remain 
undisturbed. It will be in a fixed location. No people are carrying them, and not on a cart that
moves.
- How many other teams? About a dozen or so.
- What do we do? The contest is a race. You'll want to move quickly through the different areas of
the city, while still moving slowly enough to search those areas and/or ask people for clues.
- How do we know if others have found eggs? A horn will blow that can be heard across the city.

#### Sinkhole (first search action)
```
While searching around the area large rumbling is suddenly heard from beneath the ground! The earth
begins to rumble and shake beneath your feet! 
```
Make `dexterity savings throw` against `DC 15`. Anyone one who fails is dropped into a 20 foot 
sinkhole that has formed in the middle of the street along with `seven Alootians`, taking `2d6` 
damage.

```
You and several others have fallen into a deep sinkhole. The hole is only about twenty feet deep
and with some careful use of ladders and ropes you can safely get everyone out; or you could search
for another way out.
```

Resolution: 
- Characters can use rope or ladders to assist the NPCs out of the hole; this takes 20 minutes.
- Searching with `Wisdom (Perception)` or `Investigation (Intelligence)` against `DC 20` they can 
find a tunnel into a nearby storeroom under a tailor's shop; this takes no additional time.

#### Choking (first move after Sinkhole)
```
While making your way to the next part of the city you hear a loud scream and a cry for help! When
you stop to look you see a terrified woman next to a plump man with his hands around his throat, 
and his face starting to turn blue.
```
Resolution:
- A simple `DC 15 Wisdom (Medicine)` allows the character to deftly save the man without taking
additional time.
- Otherwise, helping him is just slapping hard on his back, dislodging the food; taking 10 minutes. 

Either way, the man is grateful and tips the players off to the location of one of the eggs. In one
of the residential or the gate districts.

Note: 
If the characters already checked the residential or gate districts, use the Marketplace and 
Artisan districts instead.

#### Drowning (next search action)
```
As you search the next area with the tip from the man you saved from choking, you hear the laughter
of some children playing and splashing in the Paani river that runs through the city. But then, the
laughter turns to screams and as you look over you see the river has surged and an undercurrent is
pulling the kids into the water. The kids are gasping and flailing, barely able to keep their heads 
above water as they drift downstream.
```
Resolution: (5 children)
- A `Strength (Athletics)` against `DC 15` check will allow a character to rescue one child. This
takes 10 minutes per attempt.
- A `Wisdom (Perception)` against `DC 20` check will let the characters notice a bridge down stream
and a fisherman's boat nearby. The characters can use the net in the boat to catch the kids all at
once. This takes 10 minutes. Must be done the first round. 

Warning!
Any remaining children drown.

#### Bakery Fire (next action)
```
While [speaking to a celebrant | searching | heading to the next district] you notice an odd smell
of burnt bread. Just then you hear a huge explosion as a nearby bakery catches fire. People are 
running scared and others begin to call out "Fire! Fire!". A nearby woman runs up to you and shouts
"Please, my children and husband are in there. Save them!"
```
The Earth egg is located inside the bakery along with a man and three children.
Resolution:
- Any search for water requires a `Wisdom (Perception)` check of `DC 12` to notice a nearby well
and a bucket maker.
- If a hero enters the building, they will take `3d6` points of damage from the `heat and smoke` but
will be able to save all of the children and the man. Each hero can save two people. If repeated,
the hero will take another `3d6`.

Note: Once the players reach this point there is no chance for another group to find this egg and so
time taken for actions does not need to be tracked in this encounter.

Warning!
If no one enters the bakery to rescue the family and they only use the water source then one child dies.

```
After being saved the baker thanks you and hands you the Earth egg, saying he was 
able to save it and that you have more than earned it.
```

#### Resolution
Win:
```
After having successfully retrieved the Earth egg you return to the Temple square and present your
success. The man who split you into your groups introduces himself as Tyrza, son of Terasa the Earth
matriach and collects your egg. He let's you know that the five of you may at any time ask for
anything from the Earth matriarch within reason and she will be obliged to aquiesce. For now, you
are encouraged to enjoy the rest of the festival here in the temple district until the evening's
feast begins.
```

Lose:
```
After hearing the third horn blow you curse your luck and return to the temple quare. The man who 
separated you into your groups earlier introduces himself as Tyrza, son of Terasa, and givess you 
his condolences on the loss. While you have not earned a boon from the matriarchs, you have earned
the respect of the Order. News of the deeds you performed during the race have spread and he 
appreciates the sacrifice you've made for your fellow Alootians. If the five of you ever need 
anything, come see him at the temple and he will do all that he can to see it done. For now, you are
encouraged to enjoy the rest of the festival here in the temple district until the evening's
feast begins.
```

### Ascension
Objectives:
- Three matriarchs of earth, water, and air
- Ardor and the new peace of the Emperor
- Guild leaders 

#### Scene Setting
```
Shortly after returning from the race you stand before the Great Temple of Aloo. A stone wonder, the 
temple is carved into the face of the Great Barrier Mountain, the highest peak of the cradle of life 
and considered the pillar of Aloo. The stone steps leading to the temple's interior stretch high 
above the ground and are broken into several sections with landings. 

On the first landing you see all three matriarchs of Aloo greeting the other novitiates who ran in 
the hunt. At the base of the steps and off to the right stand a congregation of the leaders of each 
of the Guilds. To the left of the steps there is a makeshift wooden stage and a crowd jeering at the 
strangely dressed people on it.
```

Player options:
1) [Matriarchs](#Matriarchs)
2) [Guild Masters](#Guild_Masters)
3) [Ardorian Speaker](#Ardorian_Speaker)

#### Matriarchs
```
After some time waiting in line you are brought before the matriarchs of Aloo. Despite having never 
met you and being the three most powerful people in the city, they all greet you with warmth and 
delight, almost like three aunts who haven't seen their nieces and nephews in a long time. Each is 
dressed differently, representing the path of the Order she follows; one of Earth, one of Water, 
and one of Air.
```

- The Earth matriarch Terasa, a human, greets them.
- Ask which of them will belong to the Order.
  - If no one is, The Water matriarch Kamakoliki, an elf, will interrupt and bemoan today's youth.
  - The halfling Air matriarch, Petal Sunflower, scolds Kamakoliki and says the Order isn't for everyone. 
- End the conversation with a blessing of Aloo and an insistence on seeing the Ardorian Speaker.

#### Guild Masters
```
All of the guildmasters of Aloo are gathered here greeting and mingling with festival goers, mostly 
the graduating novitiates, but some of the wealthier inhabitants as well. The six of them are 
attended by a very distraught looking young human boy of about 10 or 12 who the guildmasters seem to 
delight in torturing by barking impossible orders at him.

The half-orc Merchant Captain Arturus is in a heated discussion with the gnomish Adventurer's guild 
leader, Nickelspark. Arturus is trying to convince Nickelspark that he should let him buy the 
Adventurer's guild and fold them in to the Mighty Arm of the Collective. Nickelspark doesn't look
very receptive and his eyes are searching for a way out of the conversation.

War leader Nagroth the half-ogre head of the Alootian Militia is discussing military tactics and 
longing for the "good old days" of the war with Ardor while Lucien and Mathias the two elven heads 
of the Pathfinders and Second Order insist that the war was a travesty and nothing like it should 
ever be repeated.

Tristane, the human head of the Artisan's guild stands alone not engaging much in the antics of her
colleagues.  
```

- Arturus, half-orc male, Merchant
- Nickelspark, rock gnome female, Adventurer
- Nagroth, half-ogre male, Militia
- Lucien, elf male, Pathfinder
- Mathias, elf female, Second Order
- Tristane, human female, Artisan

Resolutions:
- The encounter with Tristane is very simple and meant to communicate that, without the Artisan's
guild, no one would think about the people. Even the Order is too wrapped up into their own affairs
to care about the working class.
- The Nagroth encounter has the potential to turn violent if the characters choose the "direct"
approach. Nothing lethal, but a few punches that would actually earn the respect of Nagroth. If more
tactful, Nagroth will back down but lose that respect.
- Arturus is kind of a blow hard and the only reason people pay attention to him is his money. He is
very shrewd however and is to be feared and respected. If the characters "save" Nickelspark then he
will remember the slight.
- _Any one conversation will reveal that the true leader of the diplomatic delegation from Ardor is
Antonis, not the speaker who is merely a mouthpiece for the Emperor._

#### Ardorian Speaker
```
As you walk over to the crowd in front of the stage playing host to an elegantly dressed man, you
begin to hear the jeering and booing more clearly. You catch exclamations of "Morderer!" and 
"Raper!" and "Barbarian!" called out from people in the crowd. Once closer to the stage you can 
actually hear the man attempting to speak over the crowd. he is an emissary sent on behalf of 
Solnatus Tayvad, the Emporer of Ardor. He is attempting to read an apology and message of peace, but
the crowd seems to be more interested in making him the target of their frustration, rage, and 
grief. 

The crowd begins to throw things and people begin shoving to get to the stage. Without intervention
a fight could errupt at any second.
```

Resolutions:
- If the characters intervene they can speak to the crowd or roll a Charisma (Persuasion) against
DC 15 to try and calm them.
- If they fail, or if they do not intervene (or possibly make things worse), Akali enters onto the
stage and speaks to the crowd.
- If they succeed Akali will introduce himself and thank them

Akali Speech:
- Akali of the Imarizi
- His people also tormented by the monster Solnatus Atoom
- But Tayvad is not his father. 
- Tayvad helped his people overthrow the monster and set right the wrongs that were done 

#### Transition
```
As the sun begins to fall on the festival people begin entering into the temple. Only the honored 
of the city are granted the opportunity to feast in the temple during the Ascension, and you have 
all proven your worth today. The feast goes on for many hours but starts to die down midway thgough
the night. As the newest members of the Order's reserves, you're afforded the honor of staying the
night within the temple. As the festivities die down you are each seen to you rooms for the night. 
```

### Attack in the Night
```
You're all awakened to screams and the clanging of bells. At first you think the party has somehow
started up again and gotten out of hand, but then you recognize the screams. They're screams of 
violence and death. A young dwarf boy barges into your room and shouts "The temple is under attack!
To arms, to arms!" and then runs out as quickly as he arrived.
```

As soon as the group leaves the room they encounter [Cultists](#Cultists).

Despite the majority of shouting coming from above, Petal-Butt is heavily drawn to the [Reliquary](#Reliquary)

### Gift of Aloomatr
```
After dispatching the mysterious attackers the matriarchs enter the reliquary. Terasa speaks, "We 
saw the evil spirit. Never before has the temple been tainted by the touch of evil. Who were these
heretics, and what are you doing here in the reliquary?
```

Ad lib the rest, but the gist is the characters need to be tasked with tracking down the attackers,
and discover why they attacked and if they will return. Also, the reliquary holds the Gift of 
Aloomatr.

```
Terasa speaks again, "You (points to Petal-Butt), you seem to have a strange connection to the Gift.
As much as it pains my to see it leave the temple, something in my old bones tells me that you will
need iton your journey. Take care with it dearies, it is all we have of the Aloomatr.
```

## Encounters
### Cultists
[![](../media/cultist.png)](https://www.dndbeyond.com/monsters/black-earth-cultist)
2700 XP

### Reliquary
[![](../media/cultist.png)](https://www.dndbeyond.com/monsters/black-earth-cultist)

[![](../media/fire_elemental.png)](https://www.dndbeyond.com/monsters/fire-elemental)
5400 xp

## Sidenotes
_Remember:_

- Adjudication, Narration, and Role-Playing (making decisions).
- Narration is adjudication, scene setting, transition, and exposition.
- A scene is a basic description, with a little detail, and __an invitation to act!__
- Must also end in a transition