# Chasing Fire

## Objectives
- Cultists stole a map
- Reveal that Akali is a traitor

## Recap
- Inducted into your respective guilds
- Found the Earth egg and are owed a favor by Terasa
- City was attacked by Fire worshippers and attempted to steal an artifact
- Artifact glowed when Gee-Whilickers approached
- Tasked with tracking down the attackers

## Scenes
### Outline
- Hot pursuit or Cold Investigation
- Outpost (optional)
- Explore the Ruins (encampment or mountain pass)
- Infiltrate the enemy encampment

## A Dangerous Road
```
A few hours after the attack has finished and you've cleaned yourselves up, you find yourself in the 
presence of Terasa just before dawn. She has tasked you with tracking down the attackers to discover 
their identity, why they attacked, and if they will be back. She has also received word since the 
attack that an outpost used for early warning during the war has also been attacked last night. They 
too were attacked by fire worshippers but they don't know why.

"I don't know how the two attacks are related but it may be an interesting starting point for your 
investigation. If you wish, we can provide horses and provisions for the two day journey to the 
outpost. Or if you prefer, we know the general direction the cultist have fled to and you can pursue 
them directly."
```

Note:
Discuss how travel will work. Ensure they state how many provisions they're bringing.

Tracking:
```
The attackers headed west into the mountains. The region is rough terrain with few or no roads, 
dangerous elementals afoot, and mostly barren with little to forage. Be sure to be well supplied and
ready to fight.
```

- Danger 3
- Navigation 20
- Forage 20
- Time 12 hours ahead of party
- Gains 8 hours a day

Outpost:
```
The outpost is easy to get to with roads the entire way and few dangers. Normally it takes two days
to reach at a normal pace. But at a fast pace it can be done in about 12 hours with a forced march.
```

- Danger 1
- Navigation 10
- Forage 10
- Time 16 hours 

Travel Workflow:
- set pace (fast, normal, slow)
  - fast: 10 hours, 1 1/4
  - normal: 8 hours, 1
  - slow: 6 hours, 3/4
- roll 6d6. (morning, afternoon, evening, dusk, midnight, predawn)
- daytime encounters: morning, afternoon, evening
- make camp: navigation (survival) roll and forage (survival) roll
- night encounters (remember detection and ambush)

Rolls:
- 5 2 4 3 5 6 (2/1)
- 4 5 3 5 4 5 (1/0)
- 2 5 2 4 4 4 (2/0)
- 6 5 1 6 1 3 (3/2)
- 6 4 3 4 5 6 (1/0)

### Encounters
- [Elemental](#Elemental)
- Rope Bridge `DC 15` `Strength (Athletics)` or `Dexterity (Acrobatics)`
- Rock Slide `DC 15` `Dexterity Savings` throw or `2d10` bludgeoning 
- [Ruins](#Explore_the_Ruins)
- [Cultists](#Cultists)
- Asshole Ranger `DC 15` `Wisdom (Insight)` or `Wisdom (Survival)` or lose a day

## Outpost
```
You've traveled swiftly from Aloo, riding hard in order to investigate the attack before the 
raiders who hit Aloo get too far away. Hopefully, investigating the attack here will lead you to 
the enemy encampment. As you arrive into the outpost you're greeted by a darkly tanned man in scale 
mail who introduces himself as Gaiam.

"Greetings, Aloo told us of your coming. If there's anyway we can assist you in tracking down the 
filthy fire worshippers who attacked our home, we would be honored to assist.
```

Gaiam talking points:
- son of Terasa
- outpost is used to scout potential threats to Aloo before they arrive
- not happy about that failure
- outpost was attacked the same day, at almost the same hour as the city
- it wasn't a serious challenge, the main group fought defensively despite being the aggressors
- at some point a small splinter group broke off and dove into the ruins beneath the outpost 
- the ruins are the place of the ancients, and taboo to enter

## Explore the Ruins
From outpost:
```
Descending into the caves beneath the outpost you notice that while there's been a decent amount of 
new stone added and stalactites and stalagmites are forming, the walls and ceiling are worked stone.
Someone a long time ago occupied these caves. Perhaps the ancients rumored in stories.

You also see signs of recent activity. The commander mentioned that a splinter group of the raiders
broke off at some point during the battle. Maybe them came here? But why?
```

From the wilds:
```
While making your way through the mountains, Gee-Whilickers feels a sudden vibration as the gift begins to hum and buzz loudly. A pale blue light is escaping from where he keeps it stored. There's something about this area that has awakened it.

Not far off you see what could be the entrance to a cave.
```

```
Leaving the chase of the hunt and allowing the gift to guide you for a time you follow its light
into the opening you saw from the road. The cave opening you originally saw from the road quickly 
gives way to worked stone. While the place is obviously in ruins, you can tell it was inhabited at 
one point. But who would live out here in the wilds?

You also see signs of recent activity. Perhaps your quarry passed through this way? But why?
```

Warning:
There is a trap that requires either 1) a player to notice the statues or 2) DC 15 Wisdom (Perception).

Area 1:
````
As you make your way down the hall you make a turn right. You're looking into an antechamber 
leading into a large room with pillars that has debris and refuse strewn about. On either side of 
the antechamber you see three statues sitting in small alcoves. 
````

Area 2:
```
After making your way past the spike trap, you enter the larger room. It is chaotic with old broken
tables and ancient papers that have turned to dust. On the far end you immediately see two cultists
languishing in pain from an injury to their abdomens. "Help us", one of them calls out. The other 
one scolds his friend, "No, run fool, they'll kill us!" He begins to run off holding his wound and
not moving very quickly. His friend takes the hint and shuffles up to follow.
```

[Oblex](#Oblex)

Area 3:
```
From the looks of it this used to be a bedroom. Even at its height it must have been incredibly minimal, just a bed with a small chest at the foot.
```
```
Inside the chest is a what appears to have been a red robe, but is so tattered it's nothing but 
scraps. Underneath the scraps you find a cracked orb of glass with a tiny flicker of light.
```
```
With some coaxing the orb comes to life and a light shines from it. It's not much but you see a 
small humanoid form. It's too blurry to make out any features but it's repeating a word over and 
over, "gim gim gim gim".
```

Area 4:
```
This appears to be a library of some sort. Several shelves of books line the walls and four 
collapsed tables with a number of what used to be chairs stand in the center of the room. The room 
looks like it's been searched already as very little remains on the shelves. Almost all of the 
papers and books are nothing but dust and faded scraps. The only thing you see here that is fresh is 
a corpse in red robes. You recognize the body as your friend with the oozes. 
```

Note:
If characters insist on searching a `DC 15` `Intelligence (Investigation)` or `Wisdom (Perception)` reveals a scrap of paper with the word "gimble".

```
Searching the body reveals a map of the area with a marked location that is about half a days ride 
from the outpost and a route drawn through the mountains from the marked location to Aloo.
```

## Enemy Encampment
```
You've found them. The fire worshipping raiders who attacked your city are camped in the mountains 
and are tucked up against a steep alcove of small cliffs. Even though the actual camp is blocked 
off by a log fence you can see smoke rising from a campfire. You're just under 100 feet away but no 
cultists in sight, but you do hear the bastards laughing.
```

```
During the fight you noticed he camp seemed somewhat empty but it is nearly deserted. More 
importantly, it's not nearly large enough to house the number of raiders that attacked the city. 
However, after a little searching you notice the entrance to a cave on the north end of the camp 
which leads underground.

The passage gets a little narrow at points but then opens up. Entering the cave you see a natural 
looking cavern that branches left and right and can hear some murmurs and smell the now 
unfortunately familiar smell of sulphur burning. An elemental.
```

```
The last of the raiders now gone, you search the bodies and the area and discover a small chest. 
Inside is a map with a route marked between this encampment and Aloo that passes through the 
mountains, and a journal. Leafing through the journal you discover it belonged to the commander of 
the raid. Many of his notes are written in a strange language you're unfamiliar with but he makes 
mention of a name often, Surtr. As you continue to examine the journal a letter falls out from 
between the pages.

The letter is to the commander and reads as follows: 

Commander Valtryx,

With some work I have found what we've been looking. They keep it in a small reliquary next to the 
smaller kitchen. They don't even guard the thing, they instead prefer to obfuscate its existence. 
No matter, it should be simple enough to distract the temple guard with an attack and send a small 
team in to extract it. I recommend you initiate the attack as soon as possible. We are mere days 
away from the festival. Attacking without the cover of the festivities would be a waste of an 
opportunity.

Once the artifact is recovered, bring it to our master as soon as possible, do not wait for me. The 
delegates return to Ardor Prime after the festival and my cover as a diplomat for Ardor must be 
maintained until our master wishes it to end, even though I can scarce imagine why it will need to 
be after this. Imagine it, only one more piece after this and the plan will come to fruition!

Farewell in battle Valytryx,
Akali Ritzi
```

### Encounters
[Encampment](#Encempment)

## All Encounters
### Elemental
[![](../media/earth_elemental.png)](https://www.dndbeyond.com/monsters/earth-elemental)

### Cultists
[![](../media/cultist.png)](https://www.dndbeyond.com/monsters/black-earth-cultist)

### Oblex
[![](../media/oblex.png)](https://www.dndbeyond.com/monsters/adult-oblex)

### Encampment
[![](../media/cultist.png)](https://www.dndbeyond.com/monsters/black-earth-cultist)

[![](../media/fire_elemental.png)](https://www.dndbeyond.com/monsters/fire-elemental)

## Sidenotes
