# Digging into the Past

## Objectives
- Learn about the past
- Big reveal of Gee-Willickers resurrection
- meet farid who will point to the next dungeon

## Recap
- Found Akali and learned that the Cult is called Za'al and they excavating all over the continent
to find the rest of the artifact that created the blessing

## Into the Unknown
```
After having gathered together the supplies you'll need you make your way to the excavation site
Akali revealed to you. The excavation site is outside of normal traffic for the area as it doesn't 
lie between any major settlements and there are no precious resources of note. The land is fairly 
barren and makes for poor farming or hunting.

After two days of travel you come across the location Akali spoke of. You catch site of cart tracks
that lead off in several directions but only come from one. Following them you find a quarry being
actively worked in. However it doesn't look as if the cultists are mining it any longer, they
appear to be packing up their heavy equipment.  
```

```
After getting closer and listening to the conversations of the cultists you discover most of them 
are not true believers, they're just miners who were hired to do a job. Now that they've found the
ruins their work is done. There are some Za'al cultists around directing the work, however. You've
overheard them saying they're sending for some reinforcements as the ruins are overrun with 
monsters. You don't know how long it will take them to show up in force, but it seems that no one
is currently interested in going into the ruins until they get the backup.
```

## Level 1
[map](../media/wg_level_1.html)
Orb 1:
```
As you put the orb to the blessing you see a familiar pale blue light emerge from the orb. It shines towards the center of the room and you see a pale moon-faced human man dressed in deep ruby red robes speaking with a gnome with wild gray hair whom you can only see from behind. "Gimble", the human says excitedly, "I believe the core issue of most summonings of elementals is that they are mere projections of the elemental plane from which they come and are therefore too limited in their capabilities. I've been recently working on a series of conjurations that if successful will transplant the very essence of those planes into a vessel on ours. This would lead to a far superior elemental servant, don't you think?"
The gnome replies, "Only <burp> if you're a fucking idiot. Pure elemental essence would never survive here without constantly having to feed itself. And I don't <burp> know about you but I wouldn't want to be around when it figures out what's for dinner."
"But we would have control over the creatures!" the man replies.
"Yeah, right. <burp> You tell water not to be wet and see how that goes. Just make sure if you do decide to be a complete idiot, you take that shit waaaaaay far away from me."
The man  grunts angrily and storms off mumbling about how Gimble is impossible to work with.
The gnome turns around and you see a much older version of Gee-Whilickers! He seems to look directly at you, or rather the orb, and says, "Unfreakin believable the nerve of that guy.", and then reaching his arm out towards the orb he touches it and the light vanishes.
```

Orb 2:
```
As you put this orb to the blessing the familiar light shines forth and this time you see the moon-faced wizard from before, only now he stands to the side of a much more impressive looking man. The man is dark skinned and dressed in fine red silks and carries a mantle on his shoulders literally wreathed in flames, yet they don't seem to burn him. You also see the old gnome Gimble facing this man as he sits on some sort of throne. Despite the clear authority of the man's demeanor, Gimble is yelling at him, "Are you out of your fucking minds?! Do you really think these fire souls you've summoned, stupid name by the way idiot, are really under your control? It's the most chaotic of elements and you brought the literal, physical incarnation of its very essence on to this plane of existence?!"
The man on the throne raises his hand and speaks, "Gimble, you've proven yourself valuable in the past but these righteous servants Ignatius here has summoned have proven themselves reliable and controllable. They have obeyed our every command."
"Oh and what orders have you given them? Go kill a bunch of water tribesmen and burn their houses and shit? Big fucking deal they probably would have done that for kicks on a Friday. What happens when you give them an order they don't want to follow, huh? No, this is a huge fucking mistake and you're going to end up destroying your stupid little empire. Normally I wouldn't care, but I live here, so it's real god-damned inconvenient." Gimble turns his back on the emperor and walks away, then the light fades to nothing.
```

## Level 2
[map](../media/wg_level_2.html)
Orb 3:
```
You see Gimble sitting at a workbench tinkering with gadgets and potions as Ignatius kneels beside him pleading. "Gimble you have to help us. You were right, we never should have brought the fire souls here, they're out of control. They're making more of themselves somehow, they're spawning legions and legions of themselves. The empire is closer to complete collapse each day!" Gimble replies, "Yeah well that sounds pretty bad, and if that was all that were going on I'd take care of it this weekend, but noooooo, you had to go fucking mess with the fabric of planar reality. you want to know why they're making more of themselves? Because, and believe me I can relate to this, their entire existence on this plane is fucking torture, they're cut off from their fundamental connection to what they are. So they're lashing out trying to make that connection, because they're living fire. So any time they encounter something living in this plane they attack it. Except you can't destroy a soul, trust me I've tried, so all they end up doing is corrupting that soul into the twisted chaotic mess, much like, you guessed it, fire. So any time they run across a tree, or a pig, or a child, they just end up murdering that thing, and turning it into some horrible fire monster and the world as we know it comes one small step closer to being burnt to nothing but ash as these spirits of fire continually try to convert our universe into theirs."
"Wait, if that's true they'll destroy everything, how can we stop that?!"
"That's what I'm trying to tell you, you moron, we can't stop it. We're all dead and everything, everything that's alive is too. I give it maybe a year, year and a half. Hope it was worth it asshole." And the image disappears.
```

## A Stranger Arrives
```
After having bested the monsters and while cleaning up, you see a pale blue image of a man appear at the entrance to the chamber. His exact features are difficult to make out through the image but he is completely bald and has face tattoos of intricate geometric patterns. "Greetings. I have been studying you all for some time and I have decided to help you." 
```

Talking points:
- Farid, from a village far to the south. So far you can't even see the Asmaniji (sky lights).
- Scholar who studies the ancient world.
- Opposes the Za'al, but is familiar with their ranks.
- Knows the location of the ancient imperial capitol of the fire kingdom.
- Familiar with the artifacts they and the Za'al are searching for.
  - He knows that the Za'al have one, and that they were forged by the ancients.

Za'al
- Lead by Anblex the half-elemental (secretly Joel's mentor)
- There are three commanders: Valtryx (killed in battle with aloo or as a result of losing), Zineevius (enemy to be faced while trying to steal third piece, after death anblex arrives), and Helotus (next assault on aloo)
- Each has a variety of captains and lieutenants
- All told their strength is in the thousands
- Their largest permanent encampment lies within the mountains surrounding the Cradle of Life, far west of Aloo 