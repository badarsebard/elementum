# The Cradle of Life

## Objectives
- Find the earth orb
  - Repair the genetrix if the third piece is possessed 
- Explain the end of the world and the concept of the genetrix and the nature of surtr
  - world ends no matter what, rebuild using the genetrix
  - what are genetrix
  - the earth orb

## An Audience with Aloomatr
```
You are taken deep into the temple and brought before Aloomatr herself. Her enormous black adamantine doors tower over all give you a feeling of insignificance. What majestic secrets of creation could lie behind those doors. Could it really be the body of the goddess herself?

Terasa approaches you and says, "Alright dearies, it is time. Stand before Aloomatr and make your plea. Her herald will communicate her wishes and either grant passage or deny. But take heed, no one has ever been accepted into the Cradle."
```
Talking points:
- If anyone other than GW makes the plea, an image of Gimble appears and says, "Fuck off".
- If GW approaches and speaks to it, the doors will open.

```
The doors of Aloomatr open slowly. Those gathered around around shocked and confounded. A blast of stale air hits you and you feel a sickening in your stomach.

After entering the doors shut from behind and you are left in darkness. But only for a moment. A soft glow begins to eminate from the walls themselves as you are bathed in a calm soft light. Before you is a long tunnel that visibly descends into the mountain.
```

## Where no gnome has gone before
```
Descending deeper and deeper into the mountain you can't help but have the feeling someone is watching you. Despite this you continue down the descending tunnel until it levels and opens up into a large chamber. The chambers is obviously old, but in a better state than most of the ancient world you've seen. It looks as if it was setup to receive a large number of people and it exits out to three tunnels. At the center though, you see a familiar looking orb.

As the orb lights up you see the now familiar image of the wizard Gimble Wilkes. He speaks, "Greetings. If you're listening to this, you've probably figured out the world is fucked, and there's no fixing it. Yup, that's right. The train's pulling into the station and there's nothing any of us can do about it. Not even me. 

So why are we here? Well, normally I'd be drinking heavily right now, kicking up my feet, and watching the world literally burn. But! After a particularly good bender recently, I decided, just letting everything come to a bitter end isn't befitting enough. I need to build a monument to truly show the generations how great I am. So first, I need to make sure there are some new generations. That's where the genetrix comes in. And that's where you come in. You're going to help me build it and we're going to recreate the world after it's shuffled its mortal coil. 
```

- Each tunnel leads to a different section of the labs. 
  - One to air, one to earth, and one to water.
- Each of those is connected to a central chamber.

- genetrix is a sentient artifact with absolute control of the elements, it uses elemental orbs to exert this influence
- after the world finishes burning, and the fire souls go into hibernation, the genetrix will recreate the world

- each of the elements, earth water and air are used to rebuild the ecosystem

- burrowing into the planets core, releases the life force of the planet, causing new life to spawn

- the fire orb was used to simultaneously suppress the fire souls and could be used to undo the changes if things get out of hand and need to be restarted

- call out the centrum
 
### Air


### Earth


### Water


### Fire


### Center
