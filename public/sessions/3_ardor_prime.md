# Ardor Prime

## Objectives
- Track down Akali
- Learn he has _good_ motivation to work with cultists
- Rescue Akali's family in exchange for information about the cult

## Recap
- Discovered the enemy encampment which attacked Aloo
- Learned that Akali, the diplomat from Ardor Prime, works with the cultists

## Dramatis Personae
- Akali (human) - acting diplomat from Ardor Prime, working with the Zaal who is holding his family hostage
- Solnatus Tayvad (human) - Emperor of Ardor, ascended the throne after the overthrow of his father who lead the war against Aloo, has taken measures to make amends to the Alootians and the clans surrounding Ardor Prime. Son of Atoom.
- Archon Levyr (elf) - Chief Magistrate and principal adviser to the Solnatus, responsible for the day to day governing of Ardor Prime and its outlying regions 
- Nisha (halfling) - Thieve's guild of Ardor Prime member, tasked with keeping an eye on Akali's place in case he returns. attacks the party with some associates until she realizes they aren't cultists.
- Marcus Silverlight (elf) - a mage silk merchant who worked frequently with Akali providing scouting missions
- Brok Anvilbreaker (dwarf) - thieves guilder working out of the shaded lizard who would give more unsavory work to akali over the years
- Spark (gnome) - bookie at the fighting pits. used to make regular bets with akali until he stopped some years ago 
- Sheila Bastus - wife of Akali
- Sarah Bastus - daughter to Sheila and Akali
- Samuel Bastus - father of Sheila

## Scenes
### Outline
- Return to Aloo (optional)
- Ardor Prime and the Solnatus
- To Catch a Thief
- Rescue Rangers

## Return to Aloo (optional)
```
After discovering the journal of Valytryx, you've set off for Aloo to bring the letter from Akali 
with the incriminating information to the matriarchs. Taking the roads this time you make quick 
work of the journey. As soon as you enter the city you are brought before the three matriarchs. 
Terasa steps forward and asks, "What have you to report, dearies?"
```

Based on the information obtained the party will need to keep investigating. The fact that the 
diplomat is a traitor doesn't reveal the information they need of "Who are these cultists?", "Why
are they after the gift?" This letter has revealed some new information but not enough. Follow the
trail of Akali and seek answers.

## Ardor Prime and the Solnatus
```
The road to Ardor Prime is long but well traveled. You make the trip in short order and without 
incident. Upon arriving at the city you can't help but be impressed by the architecture. Large walls
surround the city and tall buildings the tallest you estimate almost 500 feet.

As you approach the gates you see they are open and people are busily going in and out of the city.
A gentleman with a dark complexion and pointed ears dressed in golden robes approaches you with two
standard bearers and four guards behind him. 

"Greetings travelers. The Radiant Solnatus Tayvad was informed of your coming by the honorable 
matriarch Terasa. I am Archon Levyr, his radiance's chief magistrate and principal advisor. He has
asked me to greet you in the name of Ardor Prime and to escort you into his presence. 

You are brought to the center of the city before a huge golden palace. Entering in you feel a great
sense of spaciousness as much of the palace is open to the air. After walking through the for a 
minute flanked on either side by Levyr's escort. He stops in front of you and turns to say, "We must
ask you to relinquish your weapons before entering the presence of his radiance."

==== player response ====

Having given up your weapons you are borught before the Solnatus. He is dressed in golden robes, but
unlike Levyr, he is literally glowing. He eminates a strong soft orange glow that gives you a 
feeling of warmth and comfort. He steps forward and speaks, "Greetings honored guests. What brings
you noble Alootians so far from your home?"
```

Talking points:
- Akali was brought on as a diplomat about 6 months ago
- He returned home two days ago and then asked for some time to take care of personal matters, granted
- He was once a thief but has turned over a new leaf
- Knows nothing about him working with fire cultists. He is not even aware of a fire-worshipping cult
- Akali helped overthrow the solnatus' father
- Never before expressed an interest in fire or religion
- Have permission to investigate
- Levyr will provide location of his home
- Levyr also pulls the party aside and mentions the Shaded Lizard as the place where Akali would find less reputable work

## To Catch a Thief
### Summary
This series of scenes gives the party the information they need to find Akali.

Background:
Akali was a member of the thieve's guild in Ardor Prime. Four years ago he started a protection racket on a series of outlying farms. One of them was a sheep herder by the name of Sam Bastus, who lived alone with his daughter Sheila. Sheila is a strong woman and would give Akali a ration of shit for what he was doing. Akali was unexpectedly affected by this and tried to do everything he could to make things better. At first it was simply helping with chores and work around the farm. During these encounters a fondness grew between the two of them. Eventually he devised a plan to end the racket. Three years ago Akali staged a fight between him and his crew and some gladiators from the fighting pits. Akali has been friends with one of the gladiators. He organized a band of the gladiators to "stand up" to Akali and his guild crew to end the racket. Since the revenue was never sufficient to justify the cost of fighting the gladiators, the guild backed off, leaving Akali embarassed but intact. A few months later and Akali and Sheila were married, and a year after that they had a daughter name Sarah. It was at this time, two years ago, that Akali began living on the straight and narrow. Occaisionally he would work with the guild in a legitimate capacity but mostly he worked for different merchants, especially Markus Silverlight, doing scouting work. About six months ago after having completed some work for the Solnatus he was asked by Tayvad to work as a diplomat to help repair relations with the outlying regions of the empire.

### Akali's Home
```
Arriving at the home of Akali you notice nothing immediately out of the ordinary. The home is small 
and modest. A simple lock guards the home from unwanted entrance.

Having made your way into the home it looks in good order but you immediately notice it doesn't look 
lived in. The hearth is cold with no sign of activity. There are two beds that lack sheets or 
pillows and there is a thin layer of dust in front of the door. If Akali returned to the city two 
days ago, he didn't come here.

As you continue to look about the house you're suddenly attacked as crossbow bolts are sprung! 
```

During the fight, after the first round:
```
The dark skinned halfling shouts out, 
"Come on you dirty fire worshippers! Do your worst and unleash your flames. We're snuff you out like
a candle and then the fun begins!"
```

Talking points:
- Anisha has been keeping watch on the house for the last three days waiting for Akali to return
- She thought the party was a group of the cultists Akali has been working with
- The guild is looking to Akali to pay on a debt from the time of Tayvad's ascension
- The guild assisted in the overthrow and hasn't been able to reap rewards from Akali

```
Let me tell you what. You all look like you can keep a deal. Akali knows this city inside and out, 
and more importantly he knows the guild inside and out. You bunch can probably get close to him 
easier than we can. If you find him and let us on to his location, the guild will owe you a favor.
``` 

Places to check out:
- The Shaded Lizard: a tavern off of the artisan district known to host patrons who practice less
than savory professions. Akali would frequent there in the old days when doing work for the guild.
Speak to the barkeep, Brok Anvilbreaker. 
- Marketplace: the main hub of business for the city. Akali had several contacts there who would 
provide work of a more legitimate nature. Usually scouting locations of interest in the wilds. His 
closest contact was an elf by the name of Marcus Silverlight.
- The Fighting Pits: Ardor Prime hosts events year round between gladiators. While the fights aren't
meant to be fatal, accidents happen. During the time of Atoom the gladiators were predominantly 
but Tayvad has outlawed slavery and as such most were freed. Akali would frequently place bets on 
the fights with a bookie named Spark.

### Marketplace
```
Marcus Silverlight is one of the prestigious merchants selling in the marketplace. He is one of the 
few who can provide pure elven silk cloth. Because of his status in the city he is easily found.
```

Talking points:
- Haven't seen much of Akali since he began to work for the Solnatus.
- Up until that point he'd been one of the best scouts for almost three years
- Once had a delivery of supplies he needed for a job sent to a farmhouse at the edge of the city
- North end, about a year ago 

### Shaded Lizard
```
The Shaded Lizard is easy enough to find but once you arrive you begin to wonder why someone would 
want to. The front of the building is scratched and scorched and the door looks like it's been 
broken and repaired a thousand times. The building sits between two larger ones and even during
high noon would not get much of any sun. Once inside the lack of light becomes even more apparent
and the places looks as if it a tavern lit for the night, and poorly at that. The tavern itself is
filled with all manner of people crowded around tables. While everyone is talking at a mostly normal
volume, most of you have no idea what anyone is actually saying as almost everyone here is speaking
the cant.
```

Talking points:
- Recognizes Akali's name. He helped convince the guild to assist in overthrowing Atoom. Not that it
took all that much to be honest.
- Worked plenty for the guild up until about three years ago.
- Three years ago he really cut back on how much work he was doing for the guild, and most of that
was pretty legit stuff.
- It all happened right around the time he botched an extortion job for the guild on some farmhouse
outside the city. Got his ass right beat by some sheep herder.

### Fighting Pits
```
The glory of Ardor architecture literally shines through in the arena of the fighting pits. An 
elaborate colisseum surrounds the pits and is built with an exterior of marble and silver. The 
effect this has is that the sun is constantly reflected in every direction to the point where the 
entire thing appears to glow. It's not until you go underground that you find any sort of shade.
Here you find a series of offices, one of which is marked 'Spark, Recreations Facilitator'. Being
told to enter after knocking you see an older pruny gnome holding a lit cigar. "Eh, you want to make
a bet?"
```

Talking points:
- Akali was one of Spark's best customers up until about two years ago
- Said something about having responsibilities now
- A year before that he got into a big fight with one of the gladiators. Got beat good and bloody
- Really weird part is those two had always been good friends

## Rescue Rangers
```
Having decided to checkout the farmhouse you make your way outside the city walls and north to the 
farm land there. There's only some sporadic farmsteads, but most seem to be growing things like
wheat, barley, oats, and grasses. However, you do notice one place off in the distance has a small
herd of sheep roaming. As you approach it a man suddenly emerges from a field of wheat to your right.
It is Akali. He holds up his hands and says, "Hold adventurers, let me explain. I need your help."
```

Talking points:
- Wife and daughter taken hostage by Za'al
- Have been trying to gain the trust of the cult in order to loosen the reigns
- It's worked somewhat but still unable to free them
- But with the party's help it can be done
- Will tell them anything he knows about the cult he knows in exchange

Post-Fight:
```
After having defeated the guards holding his family hostage, Akali rushes over to his wife and 
daughter and hugs them. "I can't thank you enough for helping us. I owe you everything".
```

Talking points:
- Zaal is a cult of fire worshippers who worship a fire god named Surtr
- The cult has been looking for three artifacts
- One is the blessing, they already have one, they don't know the location of the third artifact and
are excavating all over in order to find it
- He knows the location of one of them
- The cult seeks to rule the world through the use of a weapon of the ancients, fire souls
- By following Surtr, they believe he will reward them with control of this ancient weapon

## Encounter Details
[![](../media/blackguard.png)](https://www.dndbeyond.com/monsters/blackguard)

[![](../media/conjurer.png)](https://www.dndbeyond.com/monsters/conjurer)

[![](../media/swashbuckler.png)](https://www.dndbeyond.com/monsters/swashbuckler)

## Sidenotes
