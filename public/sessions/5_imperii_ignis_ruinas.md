# Imperii Ignis in Ruinas

## Objectives
- Find second piece of the Blessing
- Learn about the Genetrix and the end of the world

## Recap
- Learned that long ago the world was overrun by a plague of fire elementals that would destroy the world
- Met a man named farid who has some knowledge of the cult and the Blessing
- He thinks you will find more answers in the ruined capital of the fire kingdom

## Entrance to the Ruins
```
You are traveling on the road to the capital of the old fire kingdom. If Farid is to be trusted the
capital lies within a remote region of the world. No one travels there, no one farms there. It is a 
burnt and blighted stretch of land many believe to be cursed.

As you approach the location of the capital, the road gives way to the land and the land gives way
to a blight. The earth you move over turns to burnt glass and rock, a sandy soot where nothing 
can grow. The land slopes upward. Then, suddenly you find yourselves at the edge of a massive crater.
The crater drops steeply and you see a burnt and crumbling ruin of a city. At the center lies a large
temple, a stepped pyramid. As your eyes move up the impressive structure you notice two things:
first, the temple is made of some kind of black stone that is faintly glowing orange like coals in
a fire, and second, perched atop the temple is a gargantuan fire elemental in the shape of a bird
of prey.

Suddenly you hear a familiar voice, Farid saying, "There, that must be it, inside the temple will
be what you seek. Only something of immense elemental power could keep the firestone of the great
temple burning. And only the ancients could create something with that much elemental power."
```

Talking points: (important)
- He's never been to the capital but knows it's cursed by old spirits
  - Perhaps they could have information or even act as potential allies
- The elemental bird is a fire soul. It can feel the presence of life and feeds off it. Anything growing nearby gets consumed by this creature. That and the burning temple is how it has stayed awake all these years.
- The creature is not very intelligent. A strategy involving deception could prove advantageous.
- Direct confrontation by yourselves would most likely prove fatal, but could be possible with allies.
- Historical records indicate that the city once held a portal to the elemental planes. If it survived and the party can get it working, it could be possible to send the fire soul back to its home.

Talking points: (trivia)
- Fire stone is basically coal that never burns out. The most important structures of the old kingdom would be built from it.
- The city has one other interesting feature; a transit system. Rails run throughout the city with moving carts. There is a hub located not far from the temple in the center of the crater.

## City Overview
```
As you look out over the ruined city you notice several things other than the giant burning building 
and massive elemental that has roosted there. 

First you notice that the city is encircled by a long track which juts out from the perimeter into 
the city itself. These tracks weave their way through the former streets and buildings and are 
dotted by occasional carriages. Many of the tracks seem to congregate in different locations through 
out the city. The largest congregation is near to the temple.

Second you notice a large and unusual structure that has survived the years intact. It looks to be
the top of a large ring stood upright protruding for the roof of a large building.

Thirdly, as you look across the city streets you see occasional movement like people walking around.
On closer inspection you see what was once a market square near the edge of the city, filled with
ghostly apparitions. 
```

## Transit Hub (Heist)
Overview:
- Use the train to cause a distraction and get the phoenix to chase it
- While distracted, the rest of the party searches the temple for the artifact
- Regroup and leave the city (probably need a sacrifice of something living)

```
As you make your way towards the center of the city you feel very uneasy. Occasional movement from
the spirits catches your eye now and then while you draw closer to the looming elemental ready to
consume your very life force. You find one of the tracks you saw from outside the city which stands
on pillars meant to hold it above the streets and you follow it until it leads you to a large 
building a few blocks from the central temple.

Entering inside the station you see three different areas. The first is the platforms where 
passengers would embark and disembark the carriages The second is a row of rooms that look to be
offices. Opposite the offices you see a couple of long hallways which contain rows and rows broken
down wooden cubbies with rusted locks.
```

Platforms:
- There are two carriages located on tracks here, one is nearly destroyed
- The controls of the carriage are easy to understand, but the train is inert. DC 15 Investigation (Int) will reveal there is something missing from the console. Something long and cylindrical.

Offices:
- As party approaches the offices, they're attacked by two Allips, but can see it coming
- The head office has a ring of fire protection and a map of the rail system

Lockers:
- As party is searching lockers, surprise attack by Allips
- Only thing of note are a conductor's hat and a long cylindrical rod of some kind

Carriage controls:
- Lever that makes it move forward and back at varying speeds
- Twisting knob selects whether the carriage should fork left or right at intersections
- Rod is a wand of fire that powers the carriage

### Trains, Pains, and Elementals
Two groups: Explorers and Train Riders
Explorers look for the artifact which the Train Riders distract the phoenix.
Explorers need 10 successes

#### Explorers
Actions:
- Search blindly - 1 success
- Research - 2 successes on DC 15 Investigation (INT) or History (INT) or Religion (INT)

##### Complications:
- Trap - DC 15 Dex Save or lose next action
- Solve Puzzle - DC 15 Insight (INT) or Insight (WIS) check or lose next action
- Blocked Passage - DC 15 Athletics (STR) or lose next action

#### Train Gang
##### Train
- 8 hit points
- 4 hits - lose full speed action
- 6 hits - double chance for complication
- starts with 20 track
- starts with 10 spaces
- AC 18
##### Conductor
Actions:
- Full Speed - uses 0 spaces, uses 2 track
- Normal Speed - uses 1 space, uses 1 track
- Stop - uses 2 spaces, uses 0 track
- Turn - gain 3d6 track, uses 1 space
- Reverse - reverse direction immediately, attack of opportunity, roll complication

##### Passenger
Actions:
- Attack - gain 1 space
- Fix complication

##### Complications
- Broken component - lose 1 space each turn
- Lost footing - lose an action
- Obstruction - lose 2 spaces; must stop to fix or take 1 train damage

## Elemental Portal (Banish)
Overview:
- Examine the elemental gate
  - It uses rune stones charged with magical energy to activate
  - Charged earth stone already in the dhd
  - Bi-directional
  - Once active, will stay active for 10 minutes
- Speaking with ghost will reveal one of them knows where to find a charged fire stone
  - Will help in exchange for fixing his toy, a wooden rocking horse
- Must get the attention of the elemental and lure to gate once connected to fire plane
  - Also fight off any fire elementals that come through

```
As you make your way towards the odd structure in the city you feel very uneasy. Occasional movement 
from the spirits catches your eye now and then while you draw closer. All the while the presence of 
the looming elemental ready to consume your very life force hangs over you like a spectre. The 
structure is quite large and open inside. 

In the center you see a massive ring of stone that reaches up and past the ceiling. This must be the 
elemental portal Farid spoke of. In front of the ring structure is a small dais with a collection of 
rune stones and a space for them to fit into. One ofthe stones has a faint hum of magic to it. All 
around the structure you see the leftover ghostly denizens of the city running errands you don't
understand. All but one, a small child playing with some dolls.
```
Talking points:
- Dais - Is the control for the portal, current rune is for earth and will trigger a small fight
- Ghosts - most are unresponsive except the child; he is waiting for his parents but he wants his toy horse


## Market Square (Fight)
Overview:
- Speaking with ghosts will reveal that someone has fought the fire soul before
  - A magical orb was used and "great waves" attacked it
  - Will tell where the orb is for help, wooden rocking horse
- Use the orb to fight the phoenix
```
You make your way into the market on the edge of the city. Even being far from the temple you are 
still uneasy, and the ghosts of those who died here do not help with that. Most of the ghosts do not
speak but there are a few manning empty broken down stalls trying to sell wares disolved to dust. 
You also notice a small child sitting and crying while holding a ghostly doll.
```
Talking points:
- Vendor - tries to sell and is coy about information but money loosens the tongue
  - tells a tale of another group of people who tried to fight the great bird but lost
  - used a blue orb to summon great waves  
- Child - waiting for his parents but wants a toy while he waits
  - knows where the orb is 

## Toys R' Us
```
You've decided to help the ghost child in exchange for his help. He's provided to you the location of his
favorite toy maker, old man Tinkertot, a gnome who makes the best toys in the city. The boy
warned you though, that even though Tinkertot seems nice, lots of kids say he is a bad man. He has
lots of fun toys but he likes to play weird games and sometimes they hurt.

Making your way through the city you find the ruined toy store without a problem. When you arrive, 
most of the storefront is leveled from fire, but you do see the entrance to what looks like a basement. 
```