# The Final Act

## Objectives
- Short visit to Aloo
- Decide where next to go

## Recap
- Found a second piece to the gift of Aloomatr
- Found an orb of water which repaired the annulus
- Heard reference to Cradle of Life from the visage

## Return to Aloo
```
Returning to Aloo takes some time, but you are fortunately not sidetracked or waylaid by any misadventures. While making your way, you notice two disturbing facts. First is that the days have grown almost imperceptibly darker. The second is that during your travels from the old imperial city, you have seen no water elementals. Despite these disturbing observations, you manage to return to the city unharmed and in good time.  
```

Talking points:
- if heading straight for the temple they are allowed to immediately seek an audience with the matriarchs (Terasa, Kamakoliki, Petal Sunflower)
- otherwise shopping or other activities are acceptable
- once all are ready, the audience is granted

## Three's a crowd
```
You are brought before two of the three matriarchs of the city, Terasa and Petal Sunflower. Terasa greets you warmly, though she has a shadow of grief on her face. "Greetings and congratulations, my young friends. I know not how you managed to defeat the cult. And worse yet that the victory is soured by the sad news of the day, but rest assured your efforts have not gone unnoticed and that you have the gratitude of the matriarchs."
```

Talking Points:
- water elementals are gone from the world and the matriarch Kamakoliki has killed herself
- the cult has withdrawn itself, scouts have sent reports that there have been no sightings for weeks
  - it was assumed that the party defeated them, or at least caused the organization to collapse
- if provided the location of the cult camp, will send scouts on permanent surveillance
- entrance to the cradle is forbidden by aloomatr, as decreed by her herald
  - use of the boon will allow the party an attempt to convince the aloomatr
- if making an attempt on the cult's encampment, the city will provide supplies and a small contingent
  - no more can be spared as they are needed to defend the city and its regions while the peace with ardor prime is still tenuous