# The Great Forge of the Ancients

## Objectives
- Obtain the third piece of the gift
- Receive the earth orb
- Obtain the air orb
- Reveal Joel's mentor as primary antagonist

## Reveals
- Stars are going missing
- Disturbing dream (Zach)

## Overview
The Great Forge is situated inside the Shattered Mountain. The mountain is encircled by three plateaus used as staging grounds for the cult. The majority of the cult has begun the march towards Aloo already. Despite this, there are still formidable forces at the Forge. The only way to the summit and the command temple is to make the way up from the plateaus.

There are three available approaches. First, the direct route. Three tough fights back to back, the third being the final encounter with the reveal of Anblex. There's also the indirect approach. Farid describes a series of caverns long abandoned which connect the first and third plateaus. A distraction of the first could allow the party to enter and proceed directly to the command temple. The area is known but unguarded for fear of ancient creatures within the Forge. The third is through stealth and trickery. 

## Preparations
```
You returned from the depths of the cradle of life, heavy with the burden of knowledge that the 
world was once destroyed by the fire souls, but that the gift of Aloomatr restored the world in 
which you know. What's even stranger is that it was actually created by the gnome you've come to 
know as Gimble Wilkes. You are seen to quarters to rest from your sojourn. Mother Terasa has 
requested your presence in the morning.

Eusebius, during the night you begin to dream. You see your old master fighting off an army of 
red-robbed cultists by himself. He does well and slays many heathens, but is ultimately overcome 
by fire. As his body begins to burn you hear his screams break through the sound of battle. You 
attempt to reach him but your legs feel sluggish as if running through mud. While trying desperately 
to save your master you hear his screams turn to laughter. Not a laughter of joy but one of malice 
and mania. Stopping in confusion and fright you feel the ground give way beneath you and begin to 
fall into an endless abyss. The feeling of falling wakes you from your slumber and you realize you 
are in your chambers inside the temple. 

In the morning the five of you are brought before Terasa and Petal Sunflower. "Greetings dearies, I 
hope you've had a chance to rest and recover from your adventure. No one has ever been inside the 
cradle of life. We are curious as to what you were shown. Aloomatr willing, of course."
```
Talking points:
- what was inside?
- what did they learn?
- how will they proceed from here?
- the entire army is unavailable to accompany them to the enemy camp, however they can make use of a company (100 soldiers)
- receive Aloomatr's Tear of the Angels (earth orb)

## The Approach
```
You make your way to the home of the enemy, but this time you are not alone. The matriarchs have 
given you command of a troop of 100 soldiers, a mix of some low level paladins and fighters, but 
mostly men-at-arms. The journey is long and taken in silence.

(survival/arcana DC 23) Those of you familiar with celestial bodies notice something strange while 
making the journey. There appear to be some stars missing from the sky. None of the major 
constellations seem to be affected, but you're quite certain you can make out 2 or 3 you know to be 
missing.

As you make your final approach, a mere two days travel from your destination you are met by a 
familiar face in your command tent. Farid speaks, "Greetings friends, I see you make your way to 
the enemy camp. Please tell me you don't plan to do anything as foolish as confronting Anblex head 
on?"
```
Talking Points:
- The enemy encampment is at the summit of a place called the Shattered Mountain. The home to the Great Forge of the Ancients.
- The Forge was a seat of power for the ancients. A mountain connected directly to the elemental planes and capable of summoning the legions of elementals used as servants by the ancients.
- It is circumvallated by three defensive plateaus, the third of which is host to the Zaal temple
- Direct assault would be difficult, but possible if combined with speed. Reinforcements would arrive if fighting were to break out in any of the three plateaus. However, the upper plateaus do not maintain an active defensive posture. If the first plateau were broken through quickly enough, the party could ride mounts to the summit before a response could be formed.
- There is a tunnel that runs from the first to the third plateaus. It is not used for fear of ancient creatures that inhabit the shattered mountain.
  - The entrance lies a mile west of the start of the first plateau.
- The encampment uses passwords to verify all individuals belong to the cult. Outsiders have occasionally been permitted to enter along with an escort.
  - Passwords are passed down through the officers, from the commanders to the captains and then lieutenants.
- Farid was held prisoner here for a time. During his time here he studied the area and is familiar with its defenses
- Anblex is a formidable opponent, a powerful fighter in his own right, and blessed by Surtr himself it's said

## First Plateau
```
Approaching the Shattered Mountain from the south you see it, or rather what's left of it, rise up 
from behind the horizon. A jagged summit pierces the sky, billowing smoke and fire into the air. 
From the base of the mountain you see a deep gouge that runs around and up to the summit. The 
defensive plateaus.

As you continue to move forward, you grow suspicious of the lack of people. Estimates for the cult 
were in the thousands, yet you see naught but a scant few hundred along the portions of the plateaus 
you can see. Where have they all gone?

Despite this, there is still a significant number of forces. Farid was correct in his assessment. A 
direct assault on the plateaus would be difficult. Your forces could attack and draw out a good 
number of the cultists leaving you room to advance through the first plateau quickly, but you would 
likely meet at least some resistance along the way, and not all here are lowly novitiates.

To the west is a similar situation, albeit less drastic. In the area Farid described the location of 
the tunnel entrance you see a smaller encampment of men along with a great many hides, tanning 
equipment, and stacks and stacks of barrels. By all appearances this is a hunting and foraging party 
responsible for feeding the mountain. However, they are close enough that they could easily respond 
to any incidents at the main encampment. 
```

### Main Encampment
```
- Blackguard
- Evoker
- Cultist x3
```

[![](../media/blackguard.png)](https://www.dndbeyond.com/monsters/blackguard)

[![](../media/evoker.png)](https://www.dndbeyond.com/monsters/evoker)

[![](../media/cultist.png)](https://www.dndbeyond.com/monsters/black-earth-cultist)


### Foraging Encampment
```
- Conjurer
- Fire Elemental
```

[![](../media/conjurer.png)](https://www.dndbeyond.com/monsters/conjurer)

[![](../media/fire_elemental.png)](https://www.dndbeyond.com/monsters/fire-elemental)

## Tunnel of Love
The inside of the tunnel is host to a mind flayer (psion). It ruled the mountain and its inhabitants until the cult came. While perfectly willing to murder the party, it does provide an opportunity to temporarily ally with them in order to be freed.
```
Making your way into the tunnel you quickly lose the light. Unlike most cavernous areas you've been 
to, this tunnel is hot and dry, with a sulfurous smell that pervades the area. The passage is tight 
in places, narrowing to within only a few feet. All the while, you can't seem to shake the feeling 
you're being watched. After walking and climbing for some time you estimate you've gained a few 
hundred feet of altitude before exiting the tunnel into a large cavern. A strange foreign slime 
seems to be present on several surfaces in the cavern.

Then from the darkness you here a deep voice quietly speak, "Well, what do we have here?"
```
Talking Points:
- The mind flayer is trapped down here, unable to leave because of the temple. 
  - Unable to burrow out because of the mountain, it's not actually a mountain, but a shell grown over
  - Only escape is near the temple, and he is unable to defeat the one who rules there
- Willing to assist the characters in exchange for its freedom
- If players accept help, he will take them to the exit for the third plateau. In exchange they will be required to create a distraction sufficient enough for him to flee.
- If the players do not accept his help, he will threaten, and then attack

[![](../media/mind_flayer_psion.png)](https://www.dndbeyond.com/monsters/mind-flayer-psion)

[![](../media/ogre.png)](https://www.dndbeyond.com/monsters/ogre)

## Second Plateau
```
Entering into the second plateau you see a wide array of tents and small makeshift buildings. This 
area appears to be where the majority of what the cult needs to survive. Barracks, mess tents, 
training grounds, armorers, blacksmiths, tanners, and all manner of other tradesmen. The area seems 
much more empty than it is sized for, and you notice nearly everyone here is a tradesmen of come 
sort. The most active are the mess tent, the blacksmith, and a barracks.
```
- The second plateau holds the bulk of the forces of the cult. 
- Most are not currently here. What are available could reinforce after several minutes for the units to arm, armor, and make ranks. 
- The area is guarded minimally. 
- The primary use of the plateau is to train the members of the cult. 
- Commander Zineevius is located on this plateau at the entrance to the third plateau. Whether using trickery or speed to make their way through, Zineevius will instantly recognize the armor worn by Seb and know they are a fraud. He will attack along with his lieutenant.
  
[![](../media/warpriest.png)](https://www.dndbeyond.com/monsters/war-priest)

[![](../media/blackguard.png)](https://www.dndbeyond.com/monsters/blackguard)

## Third Plateau
```
As you enter into the third plateau it seems almost abandoned. Only the highest ranking members of 
the cult are allowed here. Officers and priests only. The temple of Zaal stands at the summit, alone 
and isolated. A few dozen yards away from the temple, the mountain falls off into a cliff that 
travels almost the entire height of the mountain.
```
Talking points:
- Anblex calmly walks out of the temple as the party approaches
  - Seb recognizes him on the spot
  - Anblex holds a red orb and it speaks, refers to GW as the anomaly, and orders Anblex to kill him
- This fight is unwinable
- The third piece of the blessing lies hidden to side of the temple, GW will notice glowing and vibration indicating such at the start of the third round
- The fight draws out a crowd but no one interferes
  - Mind flayer flies out if allied
- Farid appears after GW gets the third piece of the gift. Anblex will recognize Farid and refer to him as the traitor. Farid will tell the party to jump off the mountain and trust him.
  - He has the air orb and will use it to summon air elementals to rescue them
  
## The Second Coming
```
With the third piece of the gift in hand you line it up with the rest of the broken artifact. As the 
earth ord moves into contact with the artifact you feel a surge and spark of energy and a faint 
earthy smell. The orb begins to drain of its rich brown color and the gift begins to crackle. After 
being fully drained the orb crumbles to dust and the gift begins to hum with the familiar blue 
light. With a blinding flash you see the shape of a woman appear. There is something wrong with the 
image, it is repeated on top of itself as if there were three copies in slightly different places. 
The woman is dressed in a light toga and speaks to you in a pained voice, "The restoration is at 
hand. Bring me the corrupt one and the devastation will not be repeated. Fail and the world will 
fall forever." 
```