# Elementum

[Overview](index.md)

[Sessions]()

  * # Log
  * [Recap](logs/recap.md)
  * [All](logs/all.md)

  - - - -
  
  * # Sessions
  * [The Ascension Festival](sessions/1_ascension_festival.md)
  * [Chasing Fire](sessions/2_chasing_fire.md)
  * [Ardor Prime](sessions/3_ardor_prime.md)
  * [Digging into the Past](sessions/4_digging_past.md)
  * [Imperii Ignis in Ruinas](sessions/5_imperii_ignis_ruinas.md)
  * [Preamble](sessions/5b_preamble.md)
  * [The Shattered Mountain](sessions/6_cradle_of_life.md)
  * [The Great Forge of the Ancients](sessions/7_great_forge.md)
  * [Attack on Aloo](sessions/8_attack_aloo.md)
  
[World]()

  * [Heroes](world/characters.md)
  * [People](world/npcs.md)
  * [Organizations](world/organizations.md)

[gimmick:themechooser](Choose theme)
