# The Order and Guild of Aloo
## The Order
The governing theocracy of Aloo. Based around the worship of the elements of Air, Earth, and Water. The Order is headed by three matriarchs who each lead a Path. Each Path is aligned with a different element and serves a different purpose within the Order. The Path of Air are scholars dedicated to understanding the mechanics of the world through mathematics and natural philosophy. The Path of Water are the most typically priestly, seeking true enlightenment through finding harmony within the natural world. The Path of Earth follows a code moral pragmatism, appealing to the lower classes but also fit to govern the military might of Aloo.

## Adventurer's Guild
The adventurer's guild is sort of an oddball of a guild within the city. It supports and organizes those interested in journeying past the walls of Aloo. In exchange for the knowledge and a small portion of treasure that is acquired from adventuring, the guild will outfit its members with gear and make access to magical item purchases easier.

## The Honorable and Laudable Collective of Alootian Merchants
Aside from a taste for verbose names, the merchant's guild governs most of the trade within the city and all of the trade that happens with other nations. While the majority of the guild is made of traders, there is a section of the guild called The Mighty and Powerful Arm and Sword of the Honorable and Laudable Collective of Alootian Merchants which is responsible for the physical security of the guild's locations and caravans. Many warriors who have sought more consistency in food, pay, and work have chosen this in lieu of the freedoms of the Adventurer's guild.

## Alliance of Alootian Artisans
This guild is essentially a union for the craftsmen and skilled laborers of Aloo. It formed out of some overly heavy handed policies devised by the merchant's guild. in order to maintain the peace, the matriarchs recognized the guild and has given it the right to set labor policies for its members, including ones who are self-employed. 

## Alootian Militia
One of the smaller guilds within Aloo, the militia is a loose knit coalition of warriors, rogues, and other combat oriented classes who work and train together. While not technically a part of the military, they work exclusively for the military since the Alootian military has certain religious vows that are required as it is part of the Order.

## Pathfinders
This guild is dedicated to the exploration and protection of the lands surrounding Aloo. A favorite of rangers and druids, this guild has the most accurate maps of Alootian territory and played a crucial part during the war with Ardor as scouts and way finders.

## Second Order
The self styled "Second Order" is a guild of magical and Arcane focused members. A place for wizards, warlocks, sorcerers, arcane tricksters, and bards to congregate and discuss all things magical.
