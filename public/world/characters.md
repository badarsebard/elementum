# Player Characters
## Joel
### Eusebius "Seb" (Human Paladin)
Strong devotion to former master. A later session should reveal that his master didn't die, but 
faked his own death in order to create the cult of fire worshippers.

## Zach
### Tannis (Human Wizard)
Don't have much from him yet. Need to prod.

## Brendan
### Merovec (Tiefling Rogue)
Very money focused it seems at this point. Maybe a touch bureaucratic. 

## Aryn
### Trym (Halfling Ranger)
Urchin who grew up in the city. Probably  sought the wilds to escape?

## Scott
### Gee-Whilickers Petalbutt (Rock Gnome Bard)
Unknowingly the reincarnated self of Gimble Wilkes, the wizard who built the genetrix and recreated the 
world.