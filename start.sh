#!/bin/bash

python3 -m http.server 8080 --directory public/ --bind 127.0.0.1 &> /dev/null &
google-chrome http://localhost:8080 --new-window --user-data-dir=/tmp &> /dev/null
kill $!